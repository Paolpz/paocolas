
package paoactividad7;

public class Cola3 {
    
public static void main(String[] args) {
         Cola3 main = new Cola3();
        int[] circulo = main.Joseph(20, 45);
        for (int i = 0; i < circulo.length; i++) {
            System.out.print(i + 1 + "\t");
            System.out.println(circulo[i]);
        }
    }
 
    public int[] Joseph(int personas, int k) {
        //inicializar el circulo con todas las personas
        //vivas representadas con un 1 en su posición
        
        int[] circulo = new int[personas];
        for (int i = 0; i < personas; i++) {
            circulo[i] = 1;
        }
 
        //comienza el juego.
        int indice = 0;
        while (personas > 1) {
            //se recorre el arreglo hasta la posicion k
            for (int i = 0; i < k; i++) {
                //como es circular, si llego al final del arreglo,
                //vuelvo al principio.
                if (indice > circulo.length - 1) {
                    indice = 0;
                }
                //salteo los soldados muertos.
                while (circulo[indice] == 0) {
                    indice++;
                    if (indice > circulo.length - 1) {
                        indice = 0;
                    }
                }
                indice++;
            }
            circulo[indice - 1] = 0;
            personas = personas - 1;
        }
        return circulo;
    }
    }
    

